// Getting input via STDIN
const readline = require("readline");

const inp = readline.createInterface({
  input: process.stdin
});

const userInput = [];

inp.on("line", (data) => {
  userInput.push(data);
});

inp.on("close", () => {
  //start-here
  //Your code goes here … replace the below line with your code logic 
let numberOfInput = userInput[0];
let arr = userInput[1].split(" ")


//-----------------------------------------------

//converting array els from string to number 
arr = arr.map((val) => +val)

//extracting even number of index element to another array
let arr_even = arr.filter((val,i) => {
    return i%2 === 0;
})

arr_even.sort();


//if index is odd take it from arr , if it is even take it from arr_even
arr = arr.map((val,i) => {
  return i%2===0?arr_even[i/2]:arr[i];
})

console.log(arr);

//-----------------------------------------------

});

