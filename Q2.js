// Getting input via STDIN
const readline = require("readline");

const inp = readline.createInterface({
  input: process.stdin
});

const userInput = [];

inp.on("line", (data) => {
  userInput.push(data);
});

inp.on("close", () => {
  //start-here
  //Your code goes here … replace the below line with your code logic 
let numberOfInput = userInput[0];
let arr = userInput[1].split(" ")


//-----------------------------------------------------

arr.sort();

var map = {}

//creating key value pair -- word : occurence
for(let i= 0 ; i<arr.length ; i++){
    map[arr[i]] = ++map[arr[i]] || 1;
}

let arr2 = [];

//2D array containing [word , occurence]
for(let key in map){
    arr2.push([key , map[key]])
}

//sorting 2D array baased upon the number of occurence and digit
arr2.sort((a,b) => {
  if(a[1]==b[1]) return b[0]-a[0];
  else return b[1]-a[1];
})

//creatubg a 1d array of digit only and printing it
arr2 = arr2.map(val => val[0]);
console.log(arr2);


//-----------------------------------------------------

});

